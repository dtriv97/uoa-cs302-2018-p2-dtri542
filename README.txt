Steps to run:
1. Extract all the files and static folder into one folder
2. Open terminal in the corresponding folder and type "python server.py"
3. This should begin the server. On a browser, go to the URL localhost:8080 (or 0.0.0.0:8080) and you should be greeted by the login page
4. Enter your credentials and this should send you to the homepage of the server
5. Clicking on a user from the left user panel who is online, allows you to open the chat with them in the middle panel
6. Clicking on their username in the chat box allows you to view their profile
7. "My Profile" link at the top allows you to edit your current users profile

NOTE: This server uses the Jinja2 libary, and therefore it needs to be downloaded before running the code. All other libararies are packaged along with Python 2.7
ENJOY...

SUPPORTED CLIENTS:
- acha932
- sdhu434
- ttha702
- pjha433