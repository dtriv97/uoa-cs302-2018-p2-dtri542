listen_ip = "0.0.0.0"
listen_port = 8080

import cherrypy
import cherrypy.process.plugins
import hashlib
import urllib2
import json
import os
import os.path
import sqlite3
import math
from jinja2 import Environment, FileSystemLoader
import socket
import time
import base64
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

CURR_DIR = os.path.dirname(os.path.abspath(__file__))
env = Environment(loader=FileSystemLoader(CURR_DIR), trim_blocks=True)

def ip():
    """find ip address of current source"""
    sock=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect((socket.gethostbyname('cs302.pythonanywhere.com'), 80))
    ip = (sock.getsockname()[0])
    return ip

def location():
    """calculate location based on ip address"""
    var = ip()
    location = 2
    if ("10." == str(var)[0:3]):
        location = 0
    elif ("172." == str(var)[0:4]):
        location = 1
    else:
        location = 2
    return location


class MainApp(object):

    #CherryPy Configuration
    _cp_config = {
                    'tools.encode.on': True, 
                    'tools.encode.encoding': 'utf-8',
                    'tools.sessions.on' : 'True',
                 }                 

    def __init__(self):
        """class variables for username and password for the online session"""
        self.username=""
        self.password=""

    # If they try somewhere we don't know, catch it here and send them to the right place.
    @cherrypy.expose
    def default(self, *args, **kwargs):
        """The default page, given when we don't recognise where the request is for."""
        Page = "I don't know where you're trying to go, so have a 404 Error."
        cherrypy.response.status = 404
        return Page

    #Main page for application leads to the login form
    @cherrypy.expose
    def index(self):
        """main page redirects to the login"""
        raise cherrypy.HTTPRedirect('login')

    #Login form to validate user
    @cherrypy.expose
    def login(self, source=None):
        """login page for the server"""
        try:
            if(cherrypy.session['username'] == None): #if not logged in then continue on login page
                pass
            else:
                raise cherrypy.HTTPRedirect('/homepage')    #if logged in, redirect from login to homepage
        except KeyError:
            template = env.get_template('login.html')
            if(source==None):
                return template.render()
            else:
                return template.render(src=int(source)) #return login page with error code

    @cherrypy.expose
    def signin(self, username=None, password=None):
        """Check their name and password and send them either to the main page, or back to the main login screen."""
        error = self.authoriseUserLogin(username,password)
        if (error == '0'):  #login succesfully authenticated from login server
            cherrypy.session['username'] = username;    #store username and password for the session
            cherrypy.session['password'] = password;
            self.username = username;
            self.password = password;
            reporter = cherrypy.process.plugins.BackgroundTask(50, self.authoriseUserLogin, kwargs=dict(username=self.username, password=self.password)) #begin background thread to keep user logged in
            reporter.start()
            raise cherrypy.HTTPRedirect('/homepage')
        else:
            raise cherrypy.HTTPRedirect('/login?source='+str(error))    #return error code for unsucessful login attempt

    @cherrypy.expose
    def signout(self):
        """Logs the current user out, expires their session"""
        username = cherrypy.session.get('username')
        if (username == None):
            pass
        else:
            try:
                hashed_pass = hashlib.sha256()
                hashed_pass.update(str(cherrypy.session['password'])+str(cherrypy.session['username']))
                sock = urllib2.urlopen('http://cs302.pythonanywhere.com/logoff?username='+username+'&password='+hashed_pass.hexdigest()+'enc=0')    #logout query to login server
                htmlSource = sock.read()
                sock.close()
            except urllib2.HTTPError:
                pass
            except urllib2.URLError:
                pass

            cherrypy.lib.sessions.expire()  #expire session automatically
            raise cherrypy.HTTPRedirect('/')    #redirect to login page
    
    #User Validation from Login server
    def authoriseUserLogin(self, username, password):
        """Verifies user login from the server"""
        hashed_pass = hashlib.sha256()
        hashed_pass.update(password+username)
        print str(ip())
        print str(location())
        sock = urllib2.urlopen('http://cs302.pythonanywhere.com/report?username='+username+'&password='+hashed_pass.hexdigest()+   
            '&location='+str(location())+'&ip='+str(ip())+'&port='+str(listen_port)+'&enc=0')   #send report query for login
        htmlSource = sock.read()
        sock.close()
        return htmlSource[0]    #return error code from login server

    @cherrypy.expose
    def homepage(self):
        """Landing page after main login"""
        try:
            template = env.get_template('mainPage.html')
            return template.render(user=cherrypy.session['username'])   #return homepage
        except KeyError:    
            raise cherrypy.HTTPRedirect('/')    #routed to login page if not logged in

    @cherrypy.expose
    def retrieveUsers(self):
        try:
            '''Obtain online users'''
            hashed_pass = hashlib.sha256()
            hashed_pass.update(cherrypy.session['password']+cherrypy.session['username'])
            sock = urllib2.urlopen('http://cs302.pythonanywhere.com/getList?username='+cherrypy.session['username']+'&password='+hashed_pass.hexdigest()+'&enc=0&json=1')
            htmlSource = sock.read()
            sock.close()
            onlineUsers = json.loads(htmlSource)
            
            '''Obtain all registered users'''
            sock2 = urllib2.urlopen('http://cs302.pythonanywhere.com/listUsers')
            readAllUsers = sock2.read()
            sock2.close()
            allUserList = readAllUsers.split(",")

            '''Store all users into database'''
            db = sqlite3.connect('usersDB.db')
            cursor = db.cursor()
            cursor.execute('''UPDATE users SET online = ?''', (0,))
            db.commit()
            for i in onlineUsers:
                cursor.execute('''UPDATE users SET location = ?, ip = ?, port = ?, lastLogin = ?, online = ? WHERE username = ?''',
                    (onlineUsers[i]['location'], onlineUsers[i]['ip'], onlineUsers[i]['port'], onlineUsers[i]['lastLogin'], 1.0,onlineUsers[i]['username']))
            db.commit()
            template = env.get_template('userList.html')
            cursor.execute('''SELECT username, online from users''')    #retreive all users from database with their online status
            data = cursor.fetchall()
            cursor.close()
            db.close()
            return template.render(userList=data)
        except KeyError:
            pass     #If not logged in, nothing is shown
    
    def userOnline(self, userID):
        """Determines whether a user is shown online in the database or not"""
        db = sqlite3.connect('usersDB.db')
        cursor = db.cursor()
        cursor.execute('''SELECT online FROM users WHERE username = ?''', (userID,))
        online = cursor.fetchall()
        if(online[0][0] == 0):
            return False
        else:
            return True

    def convertTime(self, epochTime):
        """Convert a string of epoch seconds to date and time formatted string"""
        if(epochTime == 'None' or epochTime == None):   #if field is empty or not provided then return 0
            return '0'
        else:
            epochTime = float(epochTime)
            convT = time.gmtime(epochTime)
            convT = time.strftime('%d/%m/%Y %H:%M:%S', convT)
            return convT

    @cherrypy.expose    
    @cherrypy.tools.json_in()
    def receiveMessage(self):
        """API to receive message from another user"""
        input_data = cherrypy.request.json
        #Storing the message in the database
        db = sqlite3.connect('usersDB.db')
        db.text_factory = str
        cursor = db.cursor()
        cursor.execute('''INSERT INTO messages(stamp, message, sender, destination, type) VALUES(?, ?, ?, ?, ?)''', 
            (str(self.convertTime(input_data['stamp'])), input_data['message'], input_data['sender'], input_data['destination'], "text"))
        db.commit()
        cursor.close()
        db.close()
        return '0'  #return error code to user

    def getURLData(self, userID):
        db = sqlite3.connect('usersDB.db')
        cursor = db.cursor()
        cursor.execute('''SELECT ip,port FROM users WHERE username = ?''', (userID,))
        sendData = cursor.fetchall()
        return sendData

    @cherrypy.expose
    def sendMessage(self, userID=None, message=None):
        """send message to another user"""
        try:
            output_msg = {
                "stamp" : str(time.time()),
                "message": str(message),
                "sender" : cherrypy.session['username'],
                "destination" : userID
            }
            try:
                data = json.dumps(output_msg)
                userData = self.getURLData(userID)
                req = urllib2.Request('http://'+str(userData[0][0])+':'+str(userData[0][1])+'/receiveMessage', data, {'Content-Type':'application/json'})
                response = urllib2.urlopen(req)
                if(response.read() == '0'):    #if message is sent succesfully
                    db = sqlite3.connect('usersDB.db')
                    db.text_factory = str
                    cursor = db.cursor()
                    cursor.execute('''INSERT INTO messages(stamp, message, sender, destination, type) VALUES (?, ?, ?, ?, ?)''',
                        (str(self.convertTime(output_msg['stamp'])), output_msg['message'], output_msg['sender'], output_msg['destination'], "text"))   #store in database
                    db.commit()
                    cursor.close()
                    db.close()  
                    raise cherrypy.HTTPRedirect('/messaging?userID='+str(userID))
                else:
                    raise cherrypy.HTTPRedirect('/messaging?userID='+str(userID)+'&error=1')    
            except urllib2.HTTPError:
                raise cherrypy.HTTPRedirect('/messaging?userID='+str(userID)+'&error=1')
            except urllib2.URLError:
                raise cherrypy.HTTPRedirect('/messaging?userID='+str(userID)+'&error=1')
        except KeyError:
            pass

    @cherrypy.expose
    def messaging(self, userID=None, error=None):
        """Displays the chat panel on the webpage"""
        try:
            if(cherrypy.session['username'] != None):   #if logged in
                template = env.get_template('message.html')
                db = sqlite3.connect('usersDB.db')
                db.text_factory = str
                cursor = db.cursor()
                cursor.execute('''SELECT online FROM users WHERE username = ?''', (userID,))    #retrieve online status
                online = cursor.fetchone()
                cursor.execute('''SELECT stamp, message, sender, type, filename, filetype FROM messages WHERE sender = ? OR destination = ? ORDER BY stamp ASC''', (userID,userID,))
                data = cursor.fetchall()    #retrieve messages between current user and userID input
                cursor.close()
                db.close()
                return template.render(userID=userID, messageList = data, currentUser=cherrypy.session['username'], userOnline=online, error=error)
            else:
                pass
        except KeyError:
            pass

    @cherrypy.expose
    def getUserProfile(self, userID):
        """Retrieve another person's profile details"""
        db = sqlite3.connect('usersDB.db')
        cursor = db.cursor()    
        try:
            if(self.userOnline(userID)):
                try:
                    requestData = {
                        "profile_username" : str(userID),
                        "sender" : cherrypy.session['username']
                    }
                    data = json.dumps(requestData)
                    userData = self.getURLData(userID)
                    req = urllib2.Request('http://'+str(userData[0][0])+':'+str(userData[0][1])+'/getProfile', data, {'Content-Type':'application/json'})
                    response = urllib2.urlopen(req)
                    data = json.loads(response.read())
                    cursor.execute('''UPDATE userProfile SET lastUpdated = ?, fullname = ?,  position = ?, description = ?, location = ?, picture = ? WHERE username = ?''',
                        (data.get('lastUpdated'), data.get('fullname'), data.get('position'), data.get('description'), data.get('location'), data.get('picture'), str(userID),))
                    db.commit()
                except urllib2.HTTPError:
                    pass
                except urllib2.URLError:
                    pass

            cursor.execute('''SELECT * FROM userProfile WHERE username = ?''', (userID,))
            userData = cursor.fetchall()
            timestamp = self.convertTime(userData[0][0])
            cursor.close()
            db.close()  
            template = env.get_template('profile.html')
            return template.render(userData=userData, timestamp=timestamp)
        except KeyError:
            pass

    @cherrypy.expose
    def setUserProfile(self, name=None, position=None, description=None, location=None, picture=None):
        """Set profile data for current User"""
        try:
            if(name!=None):
                db = sqlite3.connect('usersDB.db')
                cursor = db.cursor()
                cursor.execute('''UPDATE userProfile SET lastUpdated = ?, fullname = ?, position = ?, description = ?, location = ?, picture = ? WHERE username = ?''', 
                    (time.time(), name, position, description, location(), picture, str(cherrypy.session['username'])))
                db.commit()
                cursor.close()
                db.close()
            template = env.get_template('myProfile.html')
            return template.render(user=cherrypy.session['username'])
        except KeyError:
            raise cherrypy.HTTPRedirect('/login')

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def getProfile(self):
        """Allows another user to get the current user's profile"""
        input_data = cherrypy.request.json
        db = sqlite3.connect('usersDB.db')
        cursor = db.cursor()
        cursor.execute('''SELECT lastUpdated, fullname, position, description, location, picture FROM userProfile WHERE username = ?''', (input_data['profile_username'],))
        profile_data = cursor.fetchall()
        output_msg = {
            'lastUpdated' : profile_data[0][0],
            'fullname' : profile_data[0][1],
            'position' : profile_data[0][2],
            'description' : profile_data[0][3],
            'location' : profile_data[0][4],
            'picture' : profile_data[0][5]
        }
        cursor.close()
        db.close()
        toSend = json.dumps(output_msg)
        return toSend


    @cherrypy.expose
    def ping(self,sender):
        """Allows external user to ping"""
        return '0'

    @cherrypy.expose
    def sendFile(self, fileToSend, userID):
        """Send a file to another user"""
        try:
            filename = str(fileToSend.filename)
            filetype = str(fileToSend.content_type.value)
            readFile = str(fileToSend.file.read())
            encodedFile = base64.b64encode(readFile)
            output_msg = {
                'sender': cherrypy.session['username'], 
                'destination': str(userID),
                'file': encodedFile,
                'filename': filename,
                'content_type': filetype,
                'stamp': time.time()
            }
            data = json.dumps(output_msg)
            userdata = self.getURLData(userID)
            req = urllib2.Request('http://'+str(userdata[0][0])+':'+str(userdata[0][1])+'/receiveFile', data, {'Content-Type':'application/json'})
            response = urllib2.urlopen(req)
            if(response.read() == '0'):    #if succesfuly file sent
                db=sqlite3.connect('usersDB.db')
                db.text_factory = str
                cursor=db.cursor()
                cursor.execute('''INSERT INTO messages(stamp, message, sender, destination, type, filename, filetype) VALUES(?, ?, ?, ?, ?, ?, ?)''', 
                    (str(self.convertTime(time.time())), readFile, str(cherrypy.session['username']), str(userID), "file", filename, filetype))
                db.commit()
                cursor.close()
                db.close()
                #download file sent into static/sent folder
                with open('static/sent/'+filename, 'wb') as f:
                    f.write(readFile)
                raise cherrypy.HTTPRedirect('/messaging?userID='+str(userID))
            else:
                raise cherrypy.HTTPRedirect('/messaging?userID='+str(userID)+'&error=2')
        except urllib2.HTTPError:
            raise cherrypy.HTTPRedirect('/messaging?userID='+str(userID)+'&error=2')
        except urllib2.URLError:
            raise cherrypy.HTTPRedirect('/messaging?userID='+str(userID)+'&error=2')
        except AttributeError:
            raise cherrypy.HTTPRedirect('/messaging?userID='+str(userID)+'&error=3')


    @cherrypy.expose
    @cherrypy.tools.json_in()
    def receiveFile(self):
        """receive another file from external user"""
        input_data = cherrypy.request.json
        db=sqlite3.connect('usersDB.db')
        db.text_factory = str
        cursor=db.cursor()
        input_data['file'] = str(base64.b64decode(input_data['file']))
        cursor.execute('''INSERT INTO messages(stamp, message, sender, destination, type, filename, filetype) VALUES(?, ?, ?, ?, ?, ?, ?)''', 
            (str(self.convertTime(input_data['stamp'])), input_data['file'], input_data['sender'], input_data['destination'], "file", input_data['filename'], input_data['content_type']))
        db.commit()
        cursor.close()
        db.close()
        #download the file received in static/downloads folder
        with open('static/downloads/'+input_data['filename'], 'wb') as f:
            f.write(input_data['file'])
        return '0'  #send error code for succesfully file received

def runMainApp():
    # Create an instance of MainApp and tell Cherrypy to send all requests under / to it. (ie all of them)
    cherrypy.tree.mount(MainApp(), '/', {
        '/':
        {   
        'tools.sessions.on' : True,
        'tools.staticdir.root' : os.path.abspath(os.getcwd()),
        'tools.staticdir.dir' : os.path.abspath(os.path.dirname(__file__))
        },
        '/static':
        {
            'tools.staticdir.on' : True,
            'tools.staticdir.root' : './static',
        }
    })

    # Tell Cherrypy to listen for connections on the configured address and port.
    cherrypy.config.update({'server.socket_host': listen_ip,
                            'server.socket_port': listen_port,
                            'engine.autoreload.on': True,
                           })

    print "========================================"
    print "University of Auckland"
    print "COMPSYS302 - Software Design Application"
    print "========================================"                       
    
    # Start the web server
    cherrypy.engine.start()

    # And stop doing anything else. Let the web server take over.
    cherrypy.engine.block()
 
#Run the function to start everything
runMainApp()
